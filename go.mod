module gitlab.com/Fryuni/go-objx

go 1.12

require github.com/stretchr/testify v1.6.1

replace github.com/stretchr/objx v0.1.0 => ./
