package objx_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/Fryuni/go-objx"
)

func TestHas(t *testing.T) {
	m := TestMap

	assert.True(t, m.Has("name"))
	assert.True(t, m.Has("address\\state"))
	assert.True(t, m.Has("numbers[4]"))

	assert.False(t, m.Has("address\\state\\nope"))
	assert.False(t, m.Has("address\\nope"))
	assert.False(t, m.Has("nope"))
	assert.False(t, m.Has("numbers[5]"))

	m = objx.Nil

	assert.False(t, m.Has("nothing"))
}
